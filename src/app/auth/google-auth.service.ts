import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Profile } from '../home/profile/profile';
import { Router } from '@angular/router';
import { PopupService } from '../popup/popup.service';

@Injectable({
  providedIn: 'root'
})
export class GoogleAuthService {
  private GAPI_CLIENT_ID = '826644810299-fqc5q3s4s1jic74hv9kf96uluc5pbsq9.apps.googleusercontent.com';
  private _profile: BehaviorSubject<Profile> = new BehaviorSubject(null);
  readonly profile$: Observable<Profile> = this._profile.asObservable();
  private _isLoaded: BehaviorSubject<boolean> = new BehaviorSubject(false);
  readonly isLoaded: Observable<boolean> = this._isLoaded.asObservable();
  readonly HOME_PAGE: string = 'home';
  //------------------------------------------------------------------------------
  constructor(
    private router: Router,
    private ngZone: NgZone,
    private popupService: PopupService
  ) {
    this.load();
   }
  //------------------------------------------------------------------------------
  load() {
    gapi.load('auth2', () => {
      gapi.auth2.init({
        client_id: this.GAPI_CLIENT_ID,
        scope: 'profile',
        fetch_basic_profile: true 
      }).then(() => {
        this._isLoaded.next(true);
      })
    });
  }
  //------------------------------------------------------------------------------
  isSignedIn() {
    return gapi.auth2.getAuthInstance().isSignedIn.get();
  }
  //-------------------------------------------------------------
  getProfile() {
    const basicProfile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
    const profile = {
      fullName: basicProfile.getName(),
      givenName: basicProfile.getGivenName(),
      lastName: basicProfile.getFamilyName(),
      imageUrl: basicProfile.getImageUrl(),
      email: basicProfile.getEmail()
    }
    this._profile.next(profile);
  }
  //-------------------------------------------------------------
  singout() {
    return new Promise((resolve, reject) => {
      gapi.auth2.getAuthInstance().signOut()
        .then(() => resolve(true))
        .catch(() => reject(false))
    })
  }
  //-------------------------------------------------------------
  renderButton() {
    gapi.signin2.render('my-signin2', {
      'scope': 'profile email',
      'width': 300,
      'height': 50,
      'longtitle': true,
      'theme': 'dark',
      'onsuccess': () => this.ngZone.run(() => {
        this.popupService.logged();
        setTimeout(() =>{
          this.router.navigate([this.HOME_PAGE])
        }, 1000)
      })
    });
  }
  //-------------------------------------------------------------
}
