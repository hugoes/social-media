import { Component, OnInit } from '@angular/core';
import { GoogleAuthService } from '../google-auth.service';
import { Router } from '@angular/router';
import { PopupService } from 'src/app/popup/popup.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //------------------------------------------------------------------------------
  private HOME_PAGE = 'home';
  loaded: boolean = false;
  subs: Subscription
  constructor(
    private router: Router,
    private googleAuthService: GoogleAuthService,
    private popupService: PopupService,
  ) { }
  //------------------------------------------------------------------------------
  ngOnInit() {
    console.log('login on init');
    this.subs = this.googleAuthService.isLoaded.subscribe((value) => {
      if(value) {
        this.googleAuthService.renderButton();
        if(this.googleAuthService.isSignedIn()) {
          this.router.navigate([this.HOME_PAGE]); 
        }
      }
    })
  }
  //------------------------------------------------------------------------------
  onClick() {
    //this.popupService.onLogout(()=> console.log('logout'));
    this.popupService.logout();
  }
}
