import { EventEmitter } from "@angular/core";

export interface PopupItemComponent {
    data: object;
    togglePopup : EventEmitter<boolean>;
    executeCallback: EventEmitter<string>
}

