import { Type } from "@angular/core";

export interface PopupItem {
    component: Type<any>;
    data: object;
}
