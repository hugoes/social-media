import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PopupItemComponent } from '../popup-item-component';

@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.css']
})
export class ShowMessageComponent implements OnInit, PopupItemComponent {
  @Input()
  data: object;
  @Output()
  togglePopup = new EventEmitter<boolean>();
  @Output()
  executeCallback = new EventEmitter<string>();
  //----------------------------------------------------
  constructor() { }
  //----------------------------------------------------
  ngOnInit() {
  }
  //----------------------------------------------------
  onClose() {
    this.togglePopup.emit(false);
  }
  //----------------------------------------------------
}
