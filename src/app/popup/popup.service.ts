import { Injectable } from '@angular/core';
import { PopupItem } from './popup-item';

import {  
  ON_LOGOUT,
  LOGGED 
} from './popup.constans';
import { Observable, BehaviorSubject } from 'rxjs';
import { ShowMessageComponent } from './show-message/show-message.component';

@Injectable({
  providedIn: 'root'
})
export class PopupService {
  private _isOpen: BehaviorSubject<boolean> = new BehaviorSubject(false);
  readonly isOpen: Observable<boolean> = this._isOpen.asObservable();
  private _popupItem: PopupItem;
  onSuccess: Function;
  //-----------------------------------------------------------------------
  constructor() {}
  //-----------------------------------------------------------------------
  get items() : PopupItem[] {
    return [
      { component: ShowMessageComponent, data: { message: 'Redireccionando'}} as PopupItem,
      { component: ShowMessageComponent, data: { message: 'Cerrando sesión'}} as PopupItem,
    ]
  }
  //-----------------------------------------------------------------------
  toggle(value: boolean) : void {
    this._isOpen.next(value);
  }
  //-------------------------------------------------------------
  logged() {
    this._popupItem = this.items[0];
    this.setOptions(null);
  }
  //-------------------------------------------------------------
  logout() {
    this._popupItem = this.items[1];
    this.setOptions(null);
  }
  //-------------------------------------------------------------
  private setOptions(onSuccess : Function) {
    //Set onSuccess
    this.onSuccess = onSuccess;
    //Set popup status
    this.toggle(true);
  }
  //-------------------------------------------------------------
  get popupItem(): PopupItem {
    return this._popupItem;
  }
  //-------------------------------------------------------------
  set popupItem(value: PopupItem) {
    this._popupItem = value;
  }
  //-------------------------------------------------------------
}
