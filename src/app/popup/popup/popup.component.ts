import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { PopupDirective } from '../popup.directive';
import { PopupService } from '../popup.service';
import { PopupItemComponent } from '../popup-item-component';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css'],
})
export class PopupComponent implements OnInit {
  @ViewChild(PopupDirective) popupDirective: PopupDirective;
  //------------------------------------------------------------
  constructor(
    private popupService: PopupService,
    private componentFactoryResolver: ComponentFactoryResolver,
  ) { }
  //------------------------------------------------------------
  ngOnInit() {
    //Subscribe to popup status changes
    this.popupService.isOpen.subscribe((value: boolean) => {
      if(value) {
        if(!this.popupService.popupItem) return;
        this.addPopupItem();
      } else {
        this.popupService.popupItem = null;
      }
    });
  }
  //------------------------------------------------------------
  private addPopupItem() {
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.popupService.popupItem.component)
    let viewContainerRef = this.popupDirective.viewContainerRef
    viewContainerRef.clear()//Clear directive
    let componentRef = viewContainerRef.createComponent(componentFactory);//Creat component
    //Get component instance
    let instance = (<PopupItemComponent>componentRef.instance);
    instance.data = this.popupService.popupItem.data;//Set input data
    instance.togglePopup.subscribe((e: boolean) => this.popupService.toggle(e));//Subscribe to togglePopup output
    instance.executeCallback.subscribe((data: any) => this.popupService.onSuccess(data));//Subscribe to executeCallback output
  }
  //------------------------------------------------------------
}
