import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupComponent } from './popup/popup.component';
import { PopupDirective } from './popup.directive';
import { ShowMessageComponent } from './show-message/show-message.component';
import { SharedModule } from '../shared/shared.module';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    PopupDirective,
    PopupComponent,
    ShowMessageComponent,
  ],
  entryComponents:[
    ShowMessageComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    SharedModule
  ],
  exports: [
    PopupComponent,
    ShowMessageComponent
  ],
  
})
export class PopupModule { }
