import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile/profile.component';
import { HomeRoutingModule } from './home-routing/home-routing.module';
import { MaterialModule } from '../material/material.module';
import { SharedModule } from '../shared/shared.module';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BaseChartComponent } from './base-chart/base-chart.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    ProfileComponent,
    BaseChartComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    MaterialModule,
    SharedModule,
    ChartsModule
  ],
})
export class HomeModule { }
