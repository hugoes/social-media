import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-base-chart',
  templateUrl: './base-chart.component.html',
  styleUrls: ['./base-chart.component.css']
})
export class BaseChartComponent implements OnInit, OnChanges {
  @Input()
  data: any[]
  constructor() { }
  ngOnInit() {}
  // lineChart
  //------------------------------------------------------------------------------
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['2012', '2013', '2014', '2015', '2016', '2017', '2018'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [65.5, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series C'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
    {data: [65.5, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
  ];
 
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  //------------------------------------------------------------------------------
  public chartHovered(e:any):void {
    console.log(e);
  }
  //------------------------------------------------------------------------------
  ngOnChanges(changes: SimpleChanges) {
    if(changes.data.currentValue) {
     this.barChartData = changes.data.currentValue;
    }
  }
  //------------------------------------------------------------------------------
}
