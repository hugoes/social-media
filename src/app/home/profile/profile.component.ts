import { Component, OnInit, NgZone, ChangeDetectorRef, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleAuthService } from 'src/app/auth/google-auth.service';
import { PopupService } from 'src/app/popup/popup.service';
import { Profile } from './profile';
import { MockDataService } from 'src/app/shared/mock-data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent implements OnInit, OnDestroy {
  private LOGIN_PAGE = 'login';
  profile: Profile;
  employees: any[];
  //------------------------------------------------------------------------------
  constructor(
    private router: Router,
    private googleAuthService: GoogleAuthService,
    private popupService: PopupService,
    private ngZone: NgZone,
    private ref: ChangeDetectorRef,
    private mockDataService: MockDataService
  ) { }
  //------------------------------------------------------------------------------
  ngOnInit() {
    this.popupService.toggle(false)

    this.mockDataService.getEmployees().subscribe(employees => {
      this.employees = employees;
      if(!this.ref['destroyed']){ 
        this.ref.detectChanges();
      }
    })
    this.googleAuthService.isLoaded.subscribe((value) => {
      if(value) {
        if(!this.googleAuthService.isSignedIn()) {
          this.ngZone.run(() => this.router.navigate([this.LOGIN_PAGE])); 
        } else {
          setTimeout(() => this.googleAuthService.getProfile(), 1000);
        }
      }
    })

    this.googleAuthService.profile$.subscribe(value => {
      if(value) {
        console.log(value);
        this.profile = value;
        if(!this.ref['destroyed']){ 
          this.ref.detectChanges();
        }
      }
    });
  }
  //------------------------------------------------------------------------------
  logout() {
    this.popupService.logout();
    setTimeout(() => {
      this.googleAuthService.singout()
        .then(() => {
          this.popupService.toggle(false);
          this.ngZone.run(() => this.router.navigate([this.LOGIN_PAGE]))
        });
    }, 2000);
  }
  //------------------------------------------------------------------------------
  ngOnDestroy() {
    console.log('profile on destroy');
    this.ref.detach();
  }
}
