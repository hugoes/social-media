export interface Profile {
    fullName: string,
    givenName: string,
    lastName: string,
    imageUrl: string,
    email: string
}
