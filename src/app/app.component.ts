import { Component } from '@angular/core';
import { GoogleAuthService } from './auth/google-auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private googleAuthService: GoogleAuthService) {}
  //------------------------------------------------------------------------------
  title = 'Social media';
  //------------------------------------------------------------------------------
}
