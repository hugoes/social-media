import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class MockDataService {

  constructor(private http: HttpClient) {}
  getEmployees() : Observable<any[]> {
    return this.http.get("assets/mockdata.json")
      .pipe(
        tap((employees:any) => {
          return employees.map((employee: any) => {
            employee.data = employee.sales_for_year.map((sale:any) => sale.value)
            employee.label = `${employee.first_name} ${employee.last_name}`
            return employee
          })
        })
      )
  }
}
