import { NgModule } from '@angular/core';
import {MatCardModule, MatInputModule, MatButtonModule, MatCheckboxModule, MatListModule} from '@angular/material';

@NgModule({
  imports: [MatCardModule, MatInputModule, MatButtonModule, MatCheckboxModule, MatListModule],
  exports: [MatCardModule, MatInputModule, MatButtonModule, MatCheckboxModule, MatListModule],
})
export class MaterialModule { }
